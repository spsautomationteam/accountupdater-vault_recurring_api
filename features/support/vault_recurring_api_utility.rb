class VaultApi

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"

#INTERNAL VARIABLES REFERNCE NUMBER AND UEL JUST FOR THIS CODE
  @reference_num
  @base_url
  @sps_merchant
# DEFINING THE CONSTRUCTOR FOR READING YAML FILE AND URL
  def initialize()
    config = YAML.load_file('config.yaml')
    @base_url = config['url']
    @sps_merchant_header_recuring=config['recuring_api']
    @sps_merchant_header_vault=config['vault_api']
    puts "@sps_merchant_header_recuring #{@sps_merchant_header_recuring}"
    puts "@@sps_merchant_header_vault #{@sps_merchant_header_vault}"
    puts "@base_url>>>>:#{@base_url}"
    set_url @base_url
  end

# DEFINING THE SETTER METHOD FOR URL, AUTHORIZATION, CONTENT TYPE, MERCHANT ,JSON BODY
  def set_url(url)
    @url = url
  end

  def set_autorization(auth)
    @authorization = auth
    #puts "authorization: #{authorization}"
  end

  def set_content_type(ctype)
    @content_type = ctype
  end

  def set_sps_merchant(spsmerchant)
    puts "######### sps_merchant #{spsmerchant}"
    case spsmerchant
      when "recurring API"
        @sps_merchant=@sps_merchant_header_recuring
      when "vault API"
        @sps_merchant=@sps_merchant_header_vault.delete('')
    end
    puts "#########************** @sps_merchant#{@sps_merchant}"
  end

  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

# HTTP POST METHOD HANDLER
  def post(path)
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_json_body.type: #{@content_type}"

    #POSTING HTTP REQUEST AND GETTING THE RESPONSE
    begin
      @response = RestClient.post full_url, @json_body, :Authorization => @authorization, :content_type => @content_type
      @reference_num = JSON.parse(@response.body)['reference']
      puts "@reference_num: #{@reference_num}"
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue Exception => err
      @response = err.response
    end
    @parsed_response = JSON.parse(@response.body)
    puts "response: #{@response}"
    #Response code
    puts "response.code : #{@response.code}"
  end

# HTTP PUT METHOD HANDLER
  def put(path)
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_json_body.type: #{@content_type}"

    #POSTING HTTP REQUEST AND GETTING THE RESPONSE
    begin
      @response = RestClient.put full_url, @json_body, :Authorization => @authorization, :SPS_Merchant => @sps_merchant, :content_type => @content_type
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue Exception => err
      @response = err.response
    end
    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "response: #{@response}"
    puts "response.code : #{@response.code}"
  end

# HTTP GET METHOD HANDLER
  def get(path)
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@authorization: #{@authorization}"
    puts "@@sps_merchant: #{@sps_merchant}"
    #POSTING HTTP REQUEST AND GETTING THE RESPONSE
    begin
      @response = RestClient.get full_url, :Authorization => @authorization, :SPS_Merchant => @sps_merchant, :content_type => @content_type
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue Exception => err
      @response = err.response
    end
    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "response: #{@response}"
    puts "response.code : #{@response.code}"

  end


# THIS METHOD WILL VERIFY THE RESPONSE CODE
  def verify_txn_code(code)
    expect(@response.code.to_s).to eql(code)
  end

# THIS METHOD WILL VERIFY THE STATUS KEY IN RESPONSE CODE (APPROVED/DECLINED)
  def verify_status(status)
    expect(@parsed_response['status']).to eql("Approved")
  end

# VERIFY THE RESPONSE KEY VALUE
  def verify_key_val(key, value)
    if (key == "code")
      expect(@response.code.to_s).to eql(value)
    else
      expect(@parsed_response[key].strip).to eql(value)
    end
  end

  def verify_response(key, value)
    expect(@parsed_response['cardData'][key].strip).to eql(value)
  end

#VERIFY THE NULL RESPONSE
  def verify_response_null()
    expect(@response.body).to eql("")
  end

end