    # Token value is generated manually for the gateway account 999999999997 and used in the below script
        # Token generation is not yet automated

    Feature: Vault API Save and Get data
      I want to test Vault API functionality


      Scenario: Check Vault API Put endpoint
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to {"cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 204
        And Response body should be null

      Scenario: Check Vault API Get endpoint
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        When I get data for /Vault/Tokens/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 200
        And Response body should contain cardType as MasterCard
        And Response body should contain cardNumber as 5499740000000065
        And Response body should contain expirationDate as 0927

      Scenario Outline: Check Vault API Put/Get endpoint for different cards
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "<CardNumber>", "expirationDate": "<ExpYear>" }
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 204
        And Response body should be null
        When I get data for /Vault/Tokens/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 200
        And Response body should contain cardType as <CardName>
        And Response body should contain cardNumber as <CardNumber>
        And Response body should contain expirationDate as <ExpYear>

        Examples:

          |CardName          |    CardNumber            |  ExpYear   |
          |  Visa            |    4111111111111111      |  1224      |
          |  Discover        |    6011000993026909      |  1125      |
          |  AmericanExpress |    371449635392376       |  1026      |
          |  MasterCard      |    5499740000000065      |  0927      |


      Scenario: Verify 'Save Credit Card information' endpoint by selecting  Content type as text/json
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to text/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 204

      Scenario: Verify 'Save Credit Card information' endpoint by selecting  Content type as application/x-www-form-urlencoded
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/x-www-form-urlencoded
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 204

        #******************************************************************************************************************************
        #Negative Scenarios
        #******************************************************************************************************************************

      Scenario: Verify 'Get Vault Data' endpoint for error code 404 by entering valid but different MID/MKEY
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 204
        Given I set SPS_merchant header to {"GatewayId": "377896836348", "GatewayKey": "C9O5B7T9Q8N7"}
        When I get data for /Vault/Tokens/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 404
        And Response body should be null

      Scenario: Verify 'Get Vault Data' endpoint for error code 404 by entering invalid token value
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        When I get data for /Vault/Tokens/1234
        Then I will verify code as 404
        And Response body should be null

      Scenario: Verify 'Get Vault Data' endpoint for error code 401 by entering invalidMID/MKEY
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header to {"GatewayId": "1234", "GatewayKey": "5678AB"}
        When I get data for /Vault/Tokens/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 401
        And I will verify errorCode as InvalidGatewayCredentials
        And I will verify errorDescription as The gateway credentials supplied are invalid

      Scenario: Verify 'Get Vault Data' endpoint for error code 401 by entering wrong Authorization value
        Given I set Authorization header as Bearer 1234
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        When I get data for /Vault/Tokens/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 401
        And I will verify errorCode as AccessTokenInvalid
        And I will verify errorDescription as The supplied access token is not associated with any authorization request

      Scenario: Verify 'Get Vault Data' endpoint for error code 401 by removing  Authorization header
        Given I set Authorization header as ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        When I get data for /Vault/Tokens/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 401
        And I will verify errorCode as InvalidHeaders
        And I will verify errorDescription as Required Authorization header not present

      Scenario: Verify 'Save Credit Card information' endpoint for error code 404 by entering valid but different MID/MKEY
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header to {"GatewayId": "377896836348", "GatewayKey": "C9O5B7T9Q8N7"}
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 404
        And Response body should be null

      Scenario: Verify 'Save Credit Card information' endpoint for error code 404 by entering invalid token value
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/1234
        Then I will verify code as 404
        And Response body should be null

      Scenario: Verify 'Save Credit Card information' endpoint for error code 401 by entering invalidMID/MKEY
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header to {"GatewayId": "1234", "GatewayKey": "1234AVCD"}
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 401
        And I will verify errorCode as InvalidGatewayCredentials
        And I will verify errorDescription as The gateway credentials supplied are invalid

      Scenario: Verify 'Save Credit Card information' endpointfor error code 401 by entering wrong Authorization value
        Given I set Authorization header as Bearer 1234
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 401
        And I will verify errorCode as AccessTokenInvalid
        And I will verify errorDescription as The supplied access token is not associated with any authorization request

      Scenario: Verify 'Save Credit Card information' endpoint for error code 400 by entering invalid expiry date
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5499740000000065", "expirationDate": "12345"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 400
        And I will verify errorCode as InvalidRequestData
        And I will verify errorDescription as creditCardInfo: Value must be valid month and year, Fromat: MMYY; Value must be valid month and year, Fromat: MMYY

      Scenario: Verify 'Save Credit Card information' endpoint for error code 400 by entering invalid card number
        Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
        And I set content-type header to application/json
        And I set SPS_merchant header for vault API
        And I set body to { "cardNumber": "5456", "expirationDate": "0927"}
        When I put data for /Vault/Tokens/CreditCard/44683b7f1c4247529a98d1f2019b79a7
        Then I will verify code as 400
        And I will verify errorCode as InvalidRequestData
        And I will verify errorDescription as creditCardInfo: The value specified is not a valid credit card number



















