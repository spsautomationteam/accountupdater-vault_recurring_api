    # payerid value is generated manually for the gateway account (101011950448) from SageExchange>Recurring module used in the below script
    # payerid generation is not yet automated

Feature: Recurring API Save and Get data
  I want to test Recurring API functionality

  @Recurring_Put
  Scenario: Check Recurring API Put endpoint
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 204
    And Response body should be null

  @Recurring_Get
  Scenario: Check Recurring API Get endpoint
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    When I get data for /Recurring/Payers/2911942/AccountInfo
    Then I will verify code as 200
    And Response body should contain cardType as MasterCard
    And Response body should contain cardNumber as 5499740000000065
    And Response body should contain expirationDate as 0927

  @Recurring_Put_Get
  Scenario Outline: Check Recurring API Put/Get endpoint for different cards
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "<CardNumber>", "expirationDate": "<ExpYear>" }
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 204
    And Response body should be null
    When I get data for /Recurring/Payers/2911942/AccountInfo
    Then I will verify code as 200
    And Response body should contain cardType as <CardName>
    And Response body should contain cardNumber as <CardNumber>
    And Response body should contain expirationDate as <ExpYear>

    Examples:

    |CardName          |    CardNumber            |  ExpYear   |
    |  Visa            |    4111111111111111      |  1224      |
    |  Discover        |    6011000993026909      |  1125      |
    |  AmericanExpress |    371449635392376       |  1026      |
    |  MasterCard      |    5499740000000065      |  0927      |


  @Recurring_text_json
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint by selecting  Content type as text/json
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to text/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 204

  @Recurring_app_x_www_urlencoded
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint by selecting  Content type as application/x-www-form-urlencoded
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/x-www-form-urlencoded
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 204

    #******************************************************************************************************************************
    #Negative Scenarios
    #******************************************************************************************************************************

  @Recurring_valid_diff_MIDMKEY
  Scenario: Verify 'Get Decrypted Account Data' endpoint for error code 404 by entering valid but different MID/MKEY
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 204
    Given I set SPS_merchant header to {"GatewayId": "999999999997", "GatewayKey": "K3QD6YWYHFD"}
    When I get data for /Recurring/Payers/2911942/AccountInfo
    Then I will verify code as 404
    And Response body should be null

  @Recurring_invalid_payerId
  Scenario: Verify 'Get Decrypted Account Data' endpoint for error code 404 by entering invalid payer id value
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    When I get data for /Recurring/Payers/1234/AccountInfo
    Then I will verify code as 404
    And Response body should be null

  @Recurring_invalid_MIDMKEY
  Scenario: Verify 'Get Decrypted Account Data' endpoint for error code 401 by entering invalidMID/MKEY
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header to {"GatewayId": "1234", "GatewayKey": "5678AB"}
    When I get data for /Recurring/Payers/2911942/AccountInfo
    Then I will verify code as 401
    And I will verify errorCode as InvalidGatewayCredentials
    And I will verify errorDescription as The gateway credentials supplied are invalid

  @Recurring_wrong_Auth
  Scenario: Verify 'Get Decrypted Account Data' endpoint for error code 401 by entering wrong Authorization value
    Given I set Authorization header as Bearer 1234
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    When I get data for /Recurring/Payers/2911942/AccountInfo
    Then I will verify code as 401
    And I will verify errorCode as AccessTokenInvalid
    And I will verify errorDescription as The supplied access token is not associated with any authorization request

  @Recurring_remove_Auth_Header
  Scenario: Verify 'Get Decrypted Account Data' endpoint for error code 401 by removing  Authorization header
    Given I set Authorization header as ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    When I get data for /Recurring/Payers/2911942/AccountInfo
    Then I will verify code as 401
    And I will verify errorCode as InvalidHeaders
    And I will verify errorDescription as Required Authorization header not present

  @Recurring_save_valid_diff_MIDMKEY
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint for error code 404 by entering valid but different MID/MKEY
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header to {"GatewayId": "999999999997", "GatewayKey": "K3QD6YWYHFD"}
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 404
    And Response body should be null

  @Recurring_save_invalid_payerId
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint for error code 404 by entering invalid payer id value
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/1234/CreditCard
    Then I will verify code as 404
    And Response body should be null

  @Recurring_save_invalid_MIDMKEY
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint for error code 401 by entering invalidMID/MKEY
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header to { "GatewayId": "1234", "GatewayKey": "abcd123 " }
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 401
    And I will verify errorCode as InvalidGatewayCredentials
    And I will verify errorDescription as The gateway credentials supplied are invalid

  @Recurring_save_wrong_AuthVal
  Scenario: Verify 'Save Decrypted Credit Card Data' endpointfor error code 401 by entering wrong Authorization value
    Given I set Authorization header as Bearer 1234
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 401
    And I will verify errorCode as AccessTokenInvalid
    And I will verify errorDescription as The supplied access token is not associated with any authorization request

  @Recurring_save_invalid_expDate
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint for error code 400 by entering invalid expiry date
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5499740000000065", "expirationDate": "12345"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 400
    And I will verify errorCode as InvalidRequestData
    And I will verify errorDescription as creditCardInfo: Value must be valid month and year, Fromat: MMYY; Value must be valid month and year, Fromat: MMYY

  @Recurring_save_invalid_cardNum
  Scenario: Verify 'Save Decrypted Credit Card Data' endpoint for error code 400 by entering invalid card number
    Given I set Authorization header as Bearer ycwbnkaftuv5upkfh6y4c6frj0whi57dtieswbckojqwsgkgcu
    And I set content-type header to application/json
    And I set SPS_merchant header for recurring API
    And I set body to { "cardNumber": "5456", "expirationDate": "0927"}
    When I put data for /Recurring/Payers/2911942/CreditCard
    Then I will verify code as 400
    And I will verify errorCode as InvalidRequestData
    And I will verify errorDescription as creditCardInfo: The value specified is not a valid credit card number



















