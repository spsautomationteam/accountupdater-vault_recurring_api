Given(/^I set Authorization header as (.*)$/) do |authorization|
  @vault_api = VaultApi.new
  @vault_api.set_autorization authorization
end


And(/^I set url as (.*)$/) do |url|
  @vault_api.set_url url
end

And(/^I set content-type header to (.*)$/) do |ctype|
  @vault_api.set_content_type ctype
end

And(/^I set SPS_merchant header for (.*)$/) do |sps_merchant|
  @vault_api.set_sps_merchant sps_merchant
end

And(/^I set SPS_merchant header to (.*)$/) do |sps_merchant|
  @vault_api.set_sps_merchant sps_merchant
end

And(/^I set body to (.*)$/) do |json|
  @vault_api.set_json_body json
end

#And(/^I post data for transaction (.*)$/) do |path|
#  @vault_api.post path
#end

And(/^I get data for (.*)$/) do |path|
  @vault_api.get path
end

And(/^I patch data for transaction (.*)$/) do |path|
  @vault_api.patch path
end

And(/^I put data for (.*)$/) do |path|
  @vault_api.put path
end

Then(/^I will verify (.*) as (.*)$/) do |key, value|
  @vault_api.verify_key_val key, value
end

Then(/^Response body should contain (.*) as (.*)/) do |key, value|
  @vault_api.verify_response key,value
end

Then(/^Response body should be null/) do
  @vault_api.verify_response_null
end

